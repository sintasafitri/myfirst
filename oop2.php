<?php 

class hewan{

	private $gerak;
	private $nafas;

	public function setGerak ($gerak){
		$this->gerak = $gerak;
	}
	public function setNafas ($nafas){
		$this->nafas = $nafas;
	}

	public function cetak ($hewan){
		echo $hewan. " bergerak dengan". $this-> gerak. ' dan bernafas dengan'. $this-> nafas. '<br>';
	}
	}
$ikan= new Hewan();
$ikan->setGerak('sirip');
$ikan->setNafas('ingsan');
$ikan->cetak('ikan');

$sapi = new Hewan();
$sapi->setGerak ('kaki');
$sapi->setNafas('paru-paru');
$sapi->cetak('sapi');

echo PHP_EOL;
	 ?>